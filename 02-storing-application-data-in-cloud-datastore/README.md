# App Dev: [Storing Application Data in Cloud Datastore][1] - Python

## Objectives

In this lab, you learn how to perform the following tasks:

* Harness Cloud Shell as your development environment
* Preview the application
* Update the application code to integrate Cloud Datastore

## Qwiklabs setup

```sh
gcloud auth list
gcloud config list project
```

## Create a virtual environment

```sh
virtualenv -p python3 venv
source venv/bin/activate
```

## Prepare the Quiz application

```sh
git clone https://github.com/GoogleCloudPlatform/training-data-analyst
cd ~/training-data-analyst/courses/developingapps/python/datastore/start
export GCLOUD_PROJECT=$DEVSHELL_PROJECT_ID
pip install -r requirements.txt
python run_server.py
```

## Examine the Quiz Application Code

## Adding Entities to Cloud Datastore

### Create an App Engine application to provision Cloud Datastore

```sh
gcloud app create --region "us-central"
```

Note: You aren't using App Engine for your web application yet. However, Cloud Datastore requires you to create an App Engine application in your project.

#### Import and use the Python Datastore module

## Adding Entities to Cloud Datastore

## Congratulations



[1]: https://www.qwiklabs.com/focuses/1076?parent=catalog