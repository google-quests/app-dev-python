# App Dev: [Storing Image and Video Files in Cloud Storage - Python][1]

## Objectives

In this lab, you learn how to perform the following tasks:

* Set up Cloud Shell as your development environment.
* Update the application code to integrate Cloud Datastore.
* Use the Quiz application to upload an image file into Cloud Storage and view the image in the Quiz

## Qwiklabs setup

```sh
gcloud config set project <YOUR-PROJECT-ID>
```

## Prepare the Quiz application

```sh
git clone https://github.com/GoogleCloudPlatform/training-data-analyst
cd ~/training-data-analyst/courses/developingapps/python/cloudstorage/start
# Configure the application, ignore any warnings
. prepare_environment.sh
```

This script file:

* Creates an App Engine application.
* Exports an environment variable, GCLOUD_PROJECT.
* Updates pip, then runs `pip install -r requirements.txt`.
* Creates entities in Cloud Datastore.
* Prints out the Google Cloud Platform Project ID.

```sh
python run_server.py
```

## Examine the Quiz application code

## Create a Cloud Storage Bucket

```sh
gsutil mb gs://$DEVSHELL_PROJECT_ID-media
export GCLOUD_BUCKET=$DEVSHELL_PROJECT_ID-media
```

## Adding objects to Cloud Storage



[1]: https://www.qwiklabs.com/focuses/1075?parent=catalog